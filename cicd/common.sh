
calculate_version() {
    # commit_sha_date as (yymmdd)
    commit_sha_date=$(git show -s --format=%cs "${CI_COMMIT_SHORT_SHA}" |sed 's/-//g;s/^.\{2\}//g')
    version="${commit_sha_date}-${CI_COMMIT_SHORT_SHA}"
    echo "${version}"
}

print_version() {
    echo "-------------------------------------------------"
    echo "------> commit_short_sha: ${CI_COMMIT_SHORT_SHA}"
    echo "------> package_version: ${version}"
    echo "-------------------------------------------------"
}

