# Base Alpine Linux based image with OpenJDK JRE only
FROM openjdk:8-jre-alpine
RUN mkdir /tmp/build/
# Add context to /tmp/build/
COPY . /tmp/build/
# copy application WAR (with libraries inside)
COPY target/springboot-h2-demo-*.jar /app.jar
# specify default command
CMD ["/usr/bin/java", "-jar", "/app.jar"]