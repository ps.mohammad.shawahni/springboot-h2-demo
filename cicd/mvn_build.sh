#!/bin/sh

# Exit on any command failure
set -e 

. "$(dirname "$0")/common.sh"
version="$(calculate_version)"

print_version

mvn versions:set -DnewVersion="$version"
mvn clean install
