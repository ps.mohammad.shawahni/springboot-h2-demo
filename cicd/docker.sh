#!/bin/sh

set -e 

. "$(dirname "$0")/common.sh"
version=$(calculate_version)

print_version

DOCKER_BUILDKIT=1 docker build -t "spr_test:${version}" -f cicd/docker/service.dockerfile .

echo "$MS_REGISTERY_PASSWORD" | docker login -u "$MS_REGISTERY_USER" --password-stdin
docker tag "spr_test:${version}" "${MS_REGISTERY_USER}/spring-h2-demo:${version}"
docker push "${MS_REGISTERY_USER}/spring-h2-demo:${version}"
docker logout