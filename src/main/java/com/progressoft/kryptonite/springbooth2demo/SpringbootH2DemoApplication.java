package com.progressoft.kryptonite.springbooth2demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@SpringBootApplication
public class SpringbootH2DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootH2DemoApplication.class, args);
    }

}

@RestController
class TestController {
    private final DRepositories dRepositories;

    TestController(DRepositories dRepositories) {
        this.dRepositories = dRepositories;
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hi there!";
    }

    @PostMapping("/ds")
    public D createD(@RequestBody D d) {
        return dRepositories.save(d);
    }

    @GetMapping("/d/{id}")
    public D getD(@PathVariable("id") Long id) {
        return dRepositories.findById(id).orElse(null);
    }

}

@Repository
interface DRepositories extends CrudRepository<D, Long> {

}


@Entity
class D {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
